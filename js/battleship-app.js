// sw=2 ts=2 et cindent 
var app = angular.module("BattleshipApp", []);

app.controller("BattleshipCtrl", function($scope, $timeout, $http) {
  var ship;

  $scope.shotsPerTurn = 1;
  $scope.gameRunning = false;
  $scope.players = [];
  $scope.messages = [];
  $scope.rowNames = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
  $scope.ships = [
    { image: 'images/carrier.png', name: 'Carrier', length: 5, hits: 0 },
    { image: 'images/battleship.png', name: 'Battleship', length: 4, hits: 0 },
    { image: 'images/submarine.png', name: 'Submarine', length: 3, hits: 0 },
    { image: 'images/cruiser.png', name: 'Cruiser', length: 3, hits: 0 },
    { image: 'images/destroyer.png', name: 'Destroyer', length: 2, hits: 0 },
    { image: 'images/sharkbait.png', name: 'Sharkbait', length: 2, hits: 0 },
  ];
  $scope.curPlayer = null;

  var addMessage = function(msg, timeout) {
    var o = {message: msg, timeout: timeout};
    if (! timeout) {
      timeout = 3000;
    };
    $scope.messages.push(o);
    $timeout(
      function () {
        var i = $scope.messages.indexOf(o);
        $scope.messages.splice(i, 1);
      },
      timeout
    );
  };

  var iterateShip = function(ship, board, horiz, c1, c2, set) {
    if (horiz) {
      for (shipCell = 0; shipCell < ship.length; shipCell++) {
        if (! set && board[c1][c2 + shipCell].hasOwnProperty('ship')) {
         return false;
        }
        else if (set) {
          board[c1][c2 + shipCell].ship = ship;
        };
      };
    }
    else {
      for (shipCell = 0; shipCell < ship.length; shipCell++) {
        if (! set && board[c2 + shipCell][c1].hasOwnProperty('ship')) {
          return false;
        }
        else if (set) {
          board[c2 + shipCell][c1].ship = ship;
        };
      };
    };
    return true;
  };

  var placeShip = function (ship, board) {
    var horiz, c1, c2;
    var done = false;

    while (!done) {
      horiz = (Math.random() > 0.5 ? true : false);
      c1 = Math.floor(Math.random() * 9);
      c2 = Math.floor(Math.random() * (9 - ship.length));

      if (iterateShip(ship, board, horiz, c1, c2, false)) {
        iterateShip(ship, board, horiz, c1, c2, true);
        done = true;
      };
    };
  };

  var checkWon = function () {
    if ($scope.ships.find( function (s) {
          return s.hits < s.length;
          })) {
      return;
    };
    addMessage('You won. Now go away.', 10000);
  };

  var calcShotsLabel = function () {
    $scope.shotLabel = ($scope.turnShotsLeft > 1) ? 'shots' : 'shot';
  };

  var beginTurn = function (p) {
    var i;

    if (typeof p === 'undefined') {
      i = $scope.players.indexOf($scope.turnPlayer);
      i++;
      if (i >= $scope.players.length) {
        i = 0;
      };
      p = $scope.players[i];
    };

    $scope.turnPlayer = p;
    //$scope.turnShotsLeft = $scope.shotsPerTurn;
    $scope.turnShotsLeft = Math.floor(Math.random() * 10) + $scope.shotsPerTurn;
    calcShotsLabel();

    $scope.coverScreen = true;
    $timeout(
      function () { $scope.coverScreen = false; },
      1000
    );
  };

  $scope.shoot = function (row, column) {
    var ship;

    console.log(row, column);
    if ($scope.curPlayer.board[row][column].shot) {
      addMessage('Quit it, you idiot.');
      return;
    };

    $scope.curPlayer.board[row][column].shot = true;
    if ($scope.curPlayer.board[row][column].ship) {
      addMessage('Hit!!');
      ship = $scope.curPlayer.board[row][column].ship;
      ship.hits++;
      console.log('Hit count on ' + ship.name + ': ' + ship.hits);
      if (ship.hits >= ship.length) {
        addMessage('You sunk the ' + ship.name + '!!!', 5000);
        ship.sunk = true;
        checkWon();
      };
    };

    $scope.turnShotsLeft--;
    if ($scope.turnShotsLeft === 0) {
      beginTurn();
    }
    else {
      calcShotsLabel();
    };
  };

  var setupPlayer = function (playerNum) {
    var x, y;
    var player = {};

    player.board = [];
    for (x = 0; x < 10; x++) {
      player.board[x] = [];
      for (y = 0; y < 10; y++) {
        player.board[x][y] = { shot: false };
      };
    };

    player.ships = [
      { image: 'images/carrier.png', name: 'Carrier', length: 5, hits: 0 },
      { image: 'images/battleship.png', name: 'Battleship', length: 4, hits: 0 },
      { image: 'images/submarine.png', name: 'Submarine', length: 3, hits: 0 },
      { image: 'images/cruiser.png', name: 'Cruiser', length: 3, hits: 0 },
      { image: 'images/destroyer.png', name: 'Destroyer', length: 2, hits: 0 },
      { image: 'images/sharkbait.png', name: 'Sharkbait', length: 2, hits: 0 },
    ];

    for (ship in player.ships) {
      placeShip(player.ships[ship], player.board);
    };

    $http.get('https://randomuser.me/api/')
      .then(
        function (data) {
          console.log('Got a new user', data);
          player.details = data.data.results[0];
          player.name = player.details.user.name.first + ' ' + player.details.user.name.last;
        }
      );

    $scope.players[playerNum] = player;
  };

  $scope.beginGame = function () {
    var i;
    document.getElementById('numPlayers').focus();
    if (! $scope.numPlayers || $scope.numPlayers < 0) {
      addMessage("You're an idiot. Select a positive number of players.");
      $scope.numPlayers = null;
      return;
    };
    if ($scope.numPlayers > 4 || Math.floor($scope.numPlayers) != $scope.numPlayers) {
      addMessage("You're a geek. Select a reasonable number of players.");
      $scope.numPlayers = null;
      return;
    };
    $scope.gameRunning = true;

    for (i = 0; i < $scope.numPlayers; i++) {
      setupPlayer(i);
    };

    beginTurn($scope.players[0]);
  };

  $scope.selectPlayer = function (player, index) {
    $scope.curPlayer = player;
  };
});
